/* eslint-disable global-require */
const sites = {
    hudebnibazar_cz: require('./hudebnibazar_cz/fetcher'),
    cyklobazar_cz: require('./cyklobazar_cz/fetcher'),
};

module.exports = sites;
