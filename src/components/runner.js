const fetchers = require('../sites/fetchers');
const { upsertOneItem } = require('./mongoFuncs');
const { sendNotifications, findItemsToNotify } = require('./notifications');
const Site = require('../models/site');

async function setSiteStart(site) {
    site.execution.lastTouch = new Date();
    site.execution.status = 'pending';
    site.execution.lastExecution = new Date();
    await site.save();
}

async function setSiteProgress(site, pageIndex) {
    site.execution.lastTouch = new Date();
    site.execution.lastPageIndex = pageIndex;
    await site.save();
}

async function setSiteFinish(site, lastItemTime) {
    site.execution.lastTouch = new Date();
    site.execution.status = 'ready';
    site.execution.lastItemTime = lastItemTime;
    site.execution.lastFinish = new Date();
    site.execution.lastPageIndex = 1;
    await site.save();
}

async function setSiteError(site) {
    site.execution.lastTouch = new Date();
    site.execution.status = 'failed';
    site.execution.lastFinish = new Date();
    await site.save();
}

function runtime(site) {
    return (site.execution.lastFinish - site.execution.lastExecution) / 1000;
}

async function runner(siteId) {
    const site = await Site.findById(siteId).exec();
    await setSiteStart(site);

    const f = new fetchers[site.site](
        site.urlForScrape,
        site.site,
        site.execution.lastItemTime,
        site.execution.concurrency,
        site.execution.lastPageIndex,
    );

    let { lastItemTime } = site.execution;

    while (f.lastItemNotFound) {
        const items = await f.getItems()
            .catch((err) => ({ err }));
        if (items.err) {
            await setSiteError(site);
            return { status: 'failed', runtime: runtime(site), items: items.err };
        }

        const promises = items.map((item) => upsertOneItem(item, site));
        await Promise.all(promises).catch((err) => console.log(err));

        if (items.length) {
            lastItemTime = items.reduce((acc, val) => (val.time > acc ? val.time : acc), lastItemTime);
        }

        await setSiteProgress(site, f.pageIndex);
    }

    await findItemsToNotify();
    await sendNotifications();

    await setSiteFinish(site, lastItemTime);
    return { status: 'finished', runtime: runtime(site) };
}

module.exports = runner;
