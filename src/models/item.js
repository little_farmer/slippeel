const mongoose = require('mongoose');

const { Schema } = mongoose;

const itemSchema = new Schema(
    {
        itemId: {
            type: 'String',
            required: true,
        },
        site: {
            type: 'String',
            required: true,
        },
        url: {
            type: 'String',
            required: true,
        },
        name: {
            type: 'String',
        },
        text: {
            type: 'String',
        },
        time: {
            type: 'Date',
        },
        label: {
            type: 'String',
        },
        price: {
            type: 'Number',
        },
        currency: {
            type: 'String',
        },
        mainCategory: {
            type: 'String',
        },
        subCategory: {
            type: 'String',
        },
        seller: {
            url: {
                type: 'String',
            },
            id: {
                type: 'String',
            },
            name: {
                type: 'String',
            },
            username: {
                type: 'String',
            },
            telephone: {
                type: 'String',
            },
            email: {
                type: 'String',
            },
            city: {
                type: 'String',
            },
            kraj: {
                type: 'String',
            },
            ratingPositive: {
                type: 'Number',
            },
            ratingNegative: {
                type: 'Number',
            },
        },
        pictures: {
            type: ['String'],
        },
        snapshots: {
            type: ['Object'],
        },
        properties: {
            maker: {
                type: 'String',
            },
            engineType: {
                type: 'String',
            },
            color: {
                type: 'String',
            },
            material: {
                type: 'String',
            },
            batteryCapacity: {
                type: 'String',
            },
            size: {
                type: 'String',
            },
            engineManufacturer: {
                type: 'String',
            },
            year: {
                type: 'String',
            },
            yearModel: {
                type: 'String',
            },
            wheelDiameter: {
                type: 'String',
            },
            travel: {
                type: 'String',
            },
            kind: {
                type: 'String',
            },
            transmissionCount: {
                type: 'String',
            },
            brakeType: {
                type: 'String',
            },
            type: {
                type: 'String',
            },
            dimension: {
                type: 'String',
            },
            category: {
                type: 'String',
            },
        },
    },
    {
        timestamps: true,
        collection: 'items',
    },
);

const Item = mongoose.model('items', itemSchema);
module.exports = Item;
