const htmlparser2 = require('htmlparser2');
const downloader = require('../../components/htmlDownloader');
const parseItem = require('./parsers/item');
const parseList = require('./parsers/list');

function fetchItem(url) {
    return downloader.fetchHtml(url, 10)
        .then((html) => [html, url]);
}

function fetchItems(urls) {
    const promises = urls.map((url) => fetchItem(url));
    return Promise
        .all(promises)
        .then((results) => results.map((e) => {
            const dom = htmlparser2.parseDocument(e[0], { normalizeWhitespace: true });
            return parseItem(dom, e[1]);
        }));
}

class Fetcher {
    constructor(searchUrl, site, lastItemTime, concurrency = 10, pageIndex = 1) {
        this.searchUrl = searchUrl;
        this.site = site;
        this.lastItemTime = lastItemTime;
        this.concurrency = concurrency;
        this.pageIndex = pageIndex;
        this.urlList = [];
        this.lastItemNotFound = true;
    }

    async getItems() {
        if (!this.urlList.length) {
            await this.fetchListOfUrls();
        }

        const items = [];
        let reducedList = this.urlList.splice(0, this.concurrency);

        while (this.lastItemNotFound && reducedList.length) {
            await fetchItems(reducedList)
                .then((newItems) => {
                    items.push(...newItems.reduce((acc, item) => {
                        if (item.time < this.lastItemTime) {
                            this.lastItemNotFound = false;
                            return acc;
                        }
                        item.site = this.site;
                        return [...acc, item];
                    }, []));
                });
            reducedList = this.urlList.splice(0, this.concurrency);
        }

        return items;
    }

    async fetchListOfUrls() {
        return downloader.fetchHtml(this.searchUrl + this.pageIndex, 10)
            .then((html) => htmlparser2.parseDocument(html, { normalizeWhitespace: true }))
            .then((dom) => parseList(dom))
            .then((urls) => {
                this.urlList = urls;
                if (!this.urlList.length) throw new Error('Fetched empty list of URLs');
                this.pageIndex += 1;
            });
    }
}

module.exports = Fetcher;
