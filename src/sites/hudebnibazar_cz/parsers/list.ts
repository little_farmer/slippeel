// eslint-disable-next-line import/no-extraneous-dependencies
const domutils = require('domutils');

function parseList(dom: HTMLElement): Array<URL> {
    return domutils
        .getElements({ class: 'InzeratBody' }, dom)
        .map((element: HTMLElement) => domutils.getElements(
            {
                class: ':not(.InzeratBody)',
                tag_name: 'a',
            },
            element,
        ))
        .map((e: HTMLElement) => new URL(domutils.getAttributeValue(e[e.length - 1], 'href'), 'https://hudebnibazar.cz/'));
}

module.exports = parseList;