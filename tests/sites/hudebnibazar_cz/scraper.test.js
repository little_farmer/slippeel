/* eslint-disable camelcase */
const fs = require('fs');
const path = require('path');
const htmlparser2 = require('htmlparser2');
const parseItem = require('../../../src/sites/hudebnibazar_cz/parsers/item');

const hudebnibazar_cz_item_1 = require('./expected/hudebnibazar_cz_item_1');
const hudebnibazar_cz_item_bug = require('./expected/hudebnibazar_cz_item_bug');

describe('Test the parser for hudebnibazar_cz', () => {
    it('parsing of dom to item object', () => {
        const html = fs.readFileSync(path.resolve(__dirname, 'html/hudebnibazar_cz_item_1.html'), { encoding: 'utf8' });
        const htmlBeta = fs.readFileSync(path.resolve(__dirname, 'html/hudebnibazar_cz_item_1_beta.html'), { encoding: 'utf8' });
        const dom = htmlparser2.parseDocument(html);
        const domBeta = htmlparser2.parseDocument(htmlBeta);
        const url = new URL('https://hudebnibazar.cz/prodam-akust-kytaru-s-fismanem-a-masivni-vrchni-deskou/ID479403/');
        const item = parseItem(dom, domBeta, url);
        expect(item).toEqual(hudebnibazar_cz_item_1);
    });

    it('parsing of dom to item object', () => {
        const html = fs.readFileSync(path.resolve(__dirname, 'html/hudebnibazar_cz_item_bug.html'), { encoding: 'utf8' });
        const htmlBeta = fs.readFileSync(path.resolve(__dirname, 'html/hudebnibazar_cz_item_bug_beta.html'), { encoding: 'utf8' });
        const dom = htmlparser2.parseDocument(html);
        const domBeta = htmlparser2.parseDocument(htmlBeta);
        const url = new URL('https://hudebnibazar.cz/sabian-xs-16-crash/ID494747/');
        const item = parseItem(dom, domBeta, url);
        expect(item).toEqual(hudebnibazar_cz_item_bug);
    });
});
