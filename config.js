require('dotenv').config();

module.exports = {
    mongo: {
        connstring: process.env.MONGODB,
        dbName: 'items',
    },
    telegramBot: {
        name: process.env.TELEGRAM_BOT_NAME,
        token: process.env.TELEGRAM_BOT_API_TOKEN,
    },
};
