require('dotenv').config({ path: '../.env' });
const colors = require('colors');
const mongoose = require('mongoose');
const startJob = require('./components/scheduler');

const CRON_STRING = '*/1 * * * *';

mongoose.connection.on('open', () => { console.log(colors.green.bold('Connected to mongo server'), new Date()); });
mongoose.connection.on('disconnected', () => { console.log(colors.yellow.bold('Disconnected from mongo server.'), new Date()); });
mongoose.connection.on('error', () => { console.error(colors.red.bold('Error with mongo server connection.'), new Date()); });

const mongoOptions = {
    heartbeatFrequencyMS: 2000,
    serverSelectionTimeoutMS: 5000,
};

mongoose.set('strictQuery', true);

mongoose.connect(process.env.MONGODB, mongoOptions)
    .then(() => {
        startJob(CRON_STRING);
    }).catch((err) => {
        console.log(err);
    });
