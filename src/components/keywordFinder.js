const Fuse = require('fuse.js');
const diacritics = require('diacritics').remove;

function findIfWanted(item, watchers) {
    const fuse = new Fuse([item], {
        includeScore: true,
        ignoreLocation: true,
        useExtendedSearch: true,
        keys: ['name', 'text'],
        threshold: 0.6,
        getFn() {
            return diacritics(Fuse.config.getFn.apply(this, arguments));
        },
    });

    return watchers.reduce((unique, watcher) => {
        const fuseResult = fuse.search(diacritics(watcher.keyword));
        if (!fuseResult.length) return unique;
        if (fuseResult[0].score <= watcher.threshold && !unique.some((obj) => obj.channelId === watcher.channelId)) {
            const foundWatcher = { ...watcher._doc };
            foundWatcher.itemId = item._id;
            return [...unique, foundWatcher];
        }
        return unique;
    }, []);
}

module.exports = { findIfWanted };
