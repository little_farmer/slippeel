const schedule = require('node-schedule');
const mongoose = require('mongoose');
const runner = require('./runner');
const Site = require('../models/site');

const PENDING_MINUTES = 5;

function startJob(cronString) {
    return schedule.scheduleJob(cronString, async () => {
        if (mongoose.connection.readyState !== 1) return;
        const sites = await Site.find({ 'execution.status': { $ne: 'stop' } });
        let site;

        const indexOF = sites.findIndex((e) => e.execution.status === 'failed');
        if (indexOF >= 0) site = sites[indexOF];

        const indexOP = sites.findIndex((e) => e.execution.status === 'pending');
        if (indexOP >= 0 && indexOF < 0) {
            const nowTs = new Date();
            const diffMinutes = Math.abs(nowTs - sites[indexOP].execution.lastTouch) / (1000 * 60);
            if (diffMinutes > PENDING_MINUTES) {
                sites[indexOP].execution.status = 'ready';
                const siteDoc = await Site.findById(sites[indexOP]._id).exec();
                siteDoc.execution.status = 'ready';
                siteDoc.save();
            } else return;
        }

        if (!site) {
            site = sites.reduce((acc, c) => (c.execution.lastExecution < acc.execution.lastExecution ? c : acc), sites[0]);
        }

        await runner(site._id);
    });
}

module.exports = startJob;
