const mongoose = require('mongoose');

const { Schema } = mongoose;

const itemSchema = new Schema(
    {
        lastNotificationTime: {
            type: 'Date',
        },
        notificationsStatus: {
            type: 'String',
        },
    },
    {
        timestamps: true,
        collection: 'configurations',
    },
);

const Configuration = mongoose.model('configurations', itemSchema);
module.exports = Configuration;
