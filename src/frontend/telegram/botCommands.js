const botCommands = [
    {
        command: 'help',
        description: 'Examples of bot usage',
    },
    {
        command: 'list',
        description: 'All your watched keywords',
    },
    {
        command: 'add',
        description: 'Add keywords, see /help',
    },

];

module.exports = botCommands;
