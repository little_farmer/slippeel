/* eslint-disable max-len */
const hudebnibazar_cz_item_1 = {
    url: 'https://hudebnibazar.cz/prodam-akust-kytaru-s-fismanem-a-masivni-vrchni-deskou/ID479403/',
    itemId: 'ID479403',
    name: 'PRODÁM akust. kytaru s FISMANEM a masívní vrchní deskou',
    text: 'PRODÁM zánovní akust. kytaru s FISMANEM. Vrchní deska masivní mahagon. Pěkný, tmavý nastroj s hezkým dřevem a parádním zvukem - mněžel má nahoře moc úzký krk (43mm) - nezvykl jsem si.  Piezzo Fishman Presys-II preamp s ladičkou, zní hezky. Masívní ladící mechaniky, drží.  Dohmat pěkný. Nesitáruje. \n'
      + 'Hraná minimálně, doma, stav nového nástroje. Jedna oděrka na krku (viz foto).  \n'
      + 'Stáří půl roku. \n'
      + 'ROZMĚRY: 40x103x12 cm\n'
      + 'Nová stála přes 5t\n'
      + 'Použijte ODPOVĚDNÍ FORMULÁŘ nebo volejte; prozvánění, SMS ani na stravenky nevracím.  OSOBNÍ PŘEDÁNÍ.!! Tohle posílat nebudu.',
    time: new Date('2023-01-17T20:56:00.000Z'),
    label: 'sell',
    price: 3900,
    currency: 'CZK',
    mainCategory: 'Kytary',
    subCategory: 'Elektroakustiky',
    seller: {
        url: 'http://hudebnibazar.cz/uzivatel/detail/?ID=72959',
        id: '72959',
        name: 'T V',
        username: 'Tondav2',
        telephone: '+420 723 165 502',
        email: 'tondavoko@volny.cz',
        city: 'Praha-východ',
        kraj: 'Středočeský kraj',
        ratingPositive: 0,
        ratingNegative: 0,
    },
    pictures: [
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2022-11/34c4d64a1f37606f9b5ec0297e54df1b.jpg',
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2022-11/f50155603226304aa525847978835dbd.jpg',
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2022-11/09aefac52958cd5e9c4b0b26881537e3.jpg',
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2022-11/12ccf0b4b49b12d9526ba805959924f2.jpg',
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2022-11/0920b398c7f4e474719bf806c3bb5e40.jpg',
    ],
};

module.exports = hudebnibazar_cz_item_1;
