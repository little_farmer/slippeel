const htmlparser2 = require('htmlparser2');
const downloader = require('../../components/htmlDownloader');
const parseItem = require('./parsers/item');
const parseList = require('./parsers/list');

function fetchItem(url) {
    const urlBeta = new URL(url);
    urlBeta.hostname = `beta.${urlBeta.hostname}`;

    const promises = [
        downloader.fetchHtml(url, 10),
        downloader.fetchHtml(urlBeta, 10),
    ];
    return Promise.all(promises)
        .then((values) => [values, url])
        .catch((err) => { throw Error(err); });
}

function fetchItems(urls) {
    const promises = urls.map((url) => fetchItem(url));
    return Promise
        .all(promises)
        .then((results) => results.map((e) => {
            const dom = htmlparser2.parseDocument(e[0][0], { normalizeWhitespace: true });
            const domBeta = htmlparser2.parseDocument(e[0][1], { normalizeWhitespace: true });
            return parseItem(dom, domBeta, e[1]);
        }))
        .catch((err) => { throw Error(err); });
}

class Fetcher {
    constructor(searchUrl, site, lastItemTime, concurrency = 2, pageIndex = 1) {
        this.searchUrl = searchUrl;
        this.site = site;
        this.lastItemTime = lastItemTime;
        this.concurrency = concurrency;
        this.pageIndex = pageIndex;
        this.urlList = [];
        this.lastItemNotFound = true;
    }

    async getItems() {
        if (!this.urlList.length) {
            await this.fetchListOfUrls();
        }

        const items = [];
        let reducedList = this.urlList.splice(0, this.concurrency);

        while (this.lastItemNotFound && reducedList.length) {
            await fetchItems(reducedList)
                .then((newItems) => {
                    items.push(...newItems.reduce((acc, item) => {
                        if (item.time < this.lastItemTime) {
                            this.lastItemNotFound = false;
                            return acc;
                        }
                        item.site = this.site;
                        return [...acc, item];
                    }, []));
                });
            reducedList = this.urlList.splice(0, this.concurrency);
        }

        return items;
    }

    async fetchListOfUrls() {
        return downloader.fetchHtml(this.searchUrl + this.pageIndex, 10)
            .then((html) => htmlparser2.parseDocument(html, { normalizeWhitespace: true }))
            .then((dom) => parseList(dom))
            .then((urls) => {
                this.urlList = urls;
                if (!this.urlList.length) throw new Error('Fetched empty list of URLs');
                this.pageIndex += 1;
            });
    }
}

module.exports = Fetcher;
