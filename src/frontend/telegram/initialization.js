const ejs = require('ejs');
const botCommands = require('./botCommands');

function initialization(bot) {
    bot.on('error', (err) => {
        console.log(err.toString());
    });

    bot.on('polling_error', (msg) => console.log(msg));

    // bot.on('message', (msg) => {
    //     console.log(
    //         'Telegram message:',
    //         msg.from.id,
    //         msg.chat.id,
    //         msg.from.username,
    //         new Date(msg.date * 1000),
    //         'command:',
    //         msg.text,
    //     );
    //     // console.log(msg);
    // });

    bot.setMyCommands(botCommands);

    bot.onText(/\/help/, async (msg) => {
        ejs.renderFile(
            `${__dirname}/templates/help.ejs`,
            (err, res) => {
                if (err) throw Error(err);
                bot.sendMessage(msg.chat.id, res, { parse_mode: 'HTML' });
            },
        );
    });
}

module.exports = initialization;
