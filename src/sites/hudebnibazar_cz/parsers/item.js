// eslint-disable-next-line import/no-extraneous-dependencies
const domutils = require('domutils');
const hudebnibazarKraje = require('../static/kraje');

function parseItem(dom, domBeta, url) {
    const host = 'https://hudebnibazar.cz';
    const item = {};
    let startIndex = 0;
    let endIndex = 0;

    // Item URL
    item.url = url.href;

    // Item ID
    item.itemId = url.pathname.split('/')[2];

    // Item Name
    item.name = domutils
        .innerText(domutils.getElementsByTagName('h1', dom))
        .trim();

    // Item advertisement text
    item.text = domutils
        .innerText(
            domutils.getElements({ class: 'InzeratText dont-break-out' }, dom),
        )
        .trim()
        .replace(/\r\n/g, '');

    // Advertisement date
    const itemDatetime = domutils
        .innerText(domutils.getElements({ class: 'InzeratZarazeno' }, dom))
        .trim();

    endIndex = itemDatetime.indexOf('pod');

    const itemDate = itemDatetime.substring(8, endIndex - 1);

    const dateParts = itemDate.split(' ')[0].split('.');
    const timeParts = itemDate.split(' ')[1].split(':');

    item.time = new Date(Date.UTC(
        dateParts[2],
        dateParts[1] - 1,
        dateParts[0],
        timeParts[0],
        timeParts[1],
    ));

    // Type of advertisement
    const itemRawLabel = domutils
        .innerText(
            domutils.getElements({ class: 'label label-nabidka label-big' }, dom),
        )
        .trim();

    switch (itemRawLabel) {
    case 'PRODÁM':
        item.label = 'sell';
        break;
    case 'KOUPÍM':
        item.label = 'buy';
        break;
    default:
        item.label = 'other';
        break;
    }

    // Price
    const itemRawPrice = domutils
        .innerText(domutils.getElements({ class: 'InzeratCena' }, dom))
        .trim()
        .split('~')[0]
        .replaceAll(/\r\n/g, '')
        .replaceAll(' ', '')
        .replace('Kč', '');

    if (itemRawPrice === 'cenadohodou') item.price = null;
    else if (!itemRawPrice) item.price = null;
    else item.price = Number(itemRawPrice.split(':')[1]);

    item.currency = 'CZK';

    // Category - from beta version advertisement page
    if (domBeta) {
        const itemCategory = domutils
            .innerText(
                domutils.getElementsByTagName(
                    'p',
                    domutils.getElements({ class: 'text-muted' }, domBeta),
                )[0].children[1],
            )
            .split('/')
            .map((e) => e.trim());

        if (itemCategory[0]) [item.mainCategory] = itemCategory;
        else item.mainCategory = null;

        if (itemCategory[1]) [, item.subCategory] = itemCategory;
        else item.subCategory = null;
    }

    // Seller whole info
    item.seller = {};

    const sellerInfo = domutils
        .innerText(domutils.getElements({ class: 'user-right' }, dom))
        .replaceAll(/\r\n/g, '')
        .replaceAll(/\s*(\s\s)/gm, ' ')
        .trim();

    // Seller url
    const urlSeller = new URL(domutils.getAttributeValue(
        domutils.getElementsByTagName(
            'a',
            domutils.getElements({ class: 'user-right' }, dom),
        )[0],
        'href',
    ), host);
    item.seller.url = urlSeller.href;

    // Seller ID
    item.seller.id = urlSeller.searchParams.get('ID');

    // Seller name
    startIndex = sellerInfo.indexOf('Jméno:') + 7;
    endIndex = sellerInfo.indexOf('(');

    item.seller.name = sellerInfo.substring(startIndex, endIndex).trim();

    // Seller username
    startIndex = sellerInfo.indexOf('(') + 1;
    endIndex = sellerInfo.indexOf(')');

    item.seller.username = sellerInfo.substring(startIndex, endIndex).trim();

    // Seller telephone
    const regexTelephone = /\+(\d|\s){4,}/;
    const telephone = regexTelephone.exec(sellerInfo);

    if (telephone) item.seller.telephone = telephone[0].trim();

    // Seller email
    startIndex = sellerInfo.indexOf('Email:') + 6;

    if (startIndex > 6) {
        endIndex = sellerInfo.indexOf('Ověřený FB profil');

        if (endIndex < 0) endIndex = sellerInfo.indexOf('Hodnocení');

        item.seller.email = sellerInfo.substring(startIndex, endIndex).trim();
    }

    // Seller location City
    startIndex = sellerInfo.indexOf('Lokalita:') + 10;
    startIndex = sellerInfo.indexOf('Lokalita:') + 10;
    endIndex = sellerInfo.indexOf('Telefon:');

    if (endIndex < 0) endIndex = sellerInfo.indexOf('Email:');
    if (endIndex < 0) endIndex = sellerInfo.indexOf('Ověřený FB profil');
    if (endIndex < 0) endIndex = sellerInfo.indexOf('Hodnocení');

    item.seller.city = sellerInfo.substring(startIndex, endIndex).trim();

    // Seller location kraj
    item.seller.kraj = hudebnibazarKraje[item.seller.city];

    // Seller rating
    startIndex = sellerInfo.indexOf('Hodnocení:') + 11;

    const sellerRating = sellerInfo
        .substring(startIndex, sellerInfo.length)
        .trim()
        .split(' ')
        .map((e) => e.substring(1));

    item.seller.ratingPositive = Number(sellerRating[0]);
    item.seller.ratingNegative = Number(sellerRating[1]);

    // Item Pictures
    const itemPictures = domutils.getElementsByTagName(
        'a',
        domutils.getElements({ class: 'InzeratObrd' }, dom),
    );

    if (itemPictures.length) {
        item.pictures = itemPictures.map((e) => host + domutils.getAttributeValue(e, 'href'));
    }

    return item;
}

module.exports = parseItem;
