const mongoose = require('mongoose');
const telegram = require('../frontend/telegram/configBot');
const Configuration = require('../models/configuration');
const Notification = require('../models/notification');
const Watcher = require('../models/watcher');
const Item = require('../models/item');

async function upsertNotif(notification) {
    await Notification.updateOne(
        {
            user: mongoose.Types.ObjectId(notification.user),
            itemId: notification.itemId,
            isSent: false,
            channel: notification.channel,
            channelId: notification.channelId,
        },
        {
            user: mongoose.Types.ObjectId(notification.user),
            itemId: notification.itemId,
            isSent: false,
            channel: notification.channel,
            channelId: notification.channelId,
            type: notification.type,
            keyword: notification.keyword,
        },
        { upsert: true },
    );
}

function searchItemsByKeyword(keyword, start, end, fuzzy = 0) {
    const match = {
        $match: { updatedAt: { $gte: start, $lt: end } },
    };

    const search = {
        $search: {
            index: 'text',
            text: {
                query: keyword,
                path: { wildcard: '*' },
            },
        },
    };

    if (fuzzy > 0) {
        search.$search.text.fuzzy = {
            maxEdits: fuzzy,
        };
    }
    return Item.aggregate([search, match]);
}

async function createNotifications(watcher, configuration, now) {
    const items = await searchItemsByKeyword(
        watcher.keyword,
        configuration.lastNotificationTime,
        now,
        watcher.fuzzy,
    );
    if (!items.length) return;
    const notifications = items.map((item) => {
        const foundWatcher = { ...watcher._doc };
        foundWatcher.itemId = item._id;
        foundWatcher.type = item.snapshots.length ? 'changed' : 'new';
        return foundWatcher;
    });
    notifications.forEach((notification) => upsertNotif(notification));
}

async function findItemsToNotify() {
    const configuration = await Configuration.findOne({});
    const now = new Date();
    const watchers = await Watcher.find({ active: true });
    const promises = watchers.map((watcher) => createNotifications(watcher, configuration, now));
    await Promise.all(promises);
    configuration.lastNotificationTime = now;
    await configuration.save();
}

async function sendSingleNotification(notification) {
    const item = await Item.findById(notification.itemId).exec();
    switch (notification.channel) {
    case 'telegram':
        if (notification.type === 'new') { telegram.sendNewItem(notification, item); }
        if (notification.type === 'changed') { telegram.sendChangedItem(notification, item); }
        break;
    default:
    }
    notification.isSent = true;
    await notification.save();
}

async function sendNotifications() {
    const notifications = await Notification.find({ isSent: false });
    const promises = notifications.map(async (notification) => {
        await sendSingleNotification(notification);
    });

    return Promise.all(promises);
}

module.exports = {
    findItemsToNotify,
    sendNotifications,
};
