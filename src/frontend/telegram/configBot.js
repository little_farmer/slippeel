const TelegramBot = require('node-telegram-bot-api');
const Queue = require('queue-promise');
const config = require('../../../config');
const commands = require('./commands');
const messages = require('./messages');
const initialization = require('./initialization');

const telegramBot = new TelegramBot(config.telegramBot.token, {
    polling: true,
});

const queue = new Queue({
    concurrent: 1,
    interval: 200,
});

initialization(telegramBot);
commands(telegramBot);

function sendChangedItem(notification, item) {
    messages.sendChangedItem(telegramBot, notification, item, queue);
}
function sendNewItem(notification, item) {
    messages.sendNewItem(telegramBot, notification, item, queue);
}

module.exports = {
    sendChangedItem,
    sendNewItem,
};
