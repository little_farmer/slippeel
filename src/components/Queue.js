class Queue {
    constructor(interval = 0) {
        this.queue = [];
        this.pendingPromise = false;
        this.interval = interval;
    }

    enqueue(promise) {
        return new Promise((resolve, reject) => {
            this.queue.push({
                promise,
                resolve,
                reject,
            });
            this.dequeue();
        });
    }

    dequeue() {
        if (this.workingOnPromise) {
            return false;
        }
        const item = this.queue.shift();
        if (!item) {
            return false;
        }
        try {
            this.workingOnPromise = true;
            item.promise()
                .then((value) => {
                    this.workingOnPromise = false;
                    item.resolve(value);
                    setTimeout(() => { this.dequeue(); }, this.interval);
                })
                .catch((err) => {
                    this.workingOnPromise = false;
                    item.reject(err);
                    setTimeout(() => { this.dequeue(); }, this.interval);
                });
        } catch (err) {
            this.workingOnPromise = false;
            item.reject(err);
            setTimeout(() => { this.dequeue(); }, this.interval);
        }
        return true;
    }
}

module.exports = Queue;
