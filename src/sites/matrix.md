| Source          | short |
| --------------- | ----- |
| hudebnibazar.cz | hbc   |
| cyklobazar.cz   | cbc   |

| Item                    | hbc | cbc |
| ----------------------- | :-: | :-: |
| itemId                  |  x  |  x  |
| site                    |  x  |  x  |
| url                     |  x  |  x  |
| name                    |  x  |  x  |
| text                    |  x  |  x  |
| time                    |  x  |  x  |
| label                   |  x  |  x  |
| price                   |  x  |  x  |
| currency                |  x  |  x  |
| mainCategory            |  x  |  x  |
| subCategory             |  x  |  x  |
| seller.url              |  x  |  x  |
| seller.id               |  x  |     |
| seller.name             |  x  |     |
| seller.username         |  x  |     |
| seller.telephone        |  x  |  x  |
| seller.email            |  x  |     |
| seller.city             |     |  x  |
| seller.kraj             |     |  x  |
| seller.ratingPositive   |     |  x  |
| seller.ratingNegative   |     |  x  |
| pictures[]              |  x  |  x  |
| snapshots[]             |  x  |  x  |
| specs.maker             |     |  x  |
| specs.engineType        |     |  x  |
| specs.color             |     |  x  |
| specs.material          |     |  x  |
| specs.batteryCapacity   |     |  x  |
| specs.size              |     |  x  |
| specs.engineManufacturer|     |  x  |
| specs.year              |     |  x  |
| specs.yearModel         |     |  x  |
| specs.wheelDiameter     |     |  x  |
| specs.travel            |     |  x  |
| specs.kind              |     |  x  |
| specs.transmissionCount |     |  x  |
| specs.brakeType         |     |  x  |
| specs.type              |     |  x  |
| specs.dimension         |     |  x  |
| specs.category          |     |  x  |
