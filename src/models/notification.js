const mongoose = require('mongoose');

const { Schema } = mongoose;

const notificationSchema = new Schema(
    {
        user: {
            type: 'ObjectId',
            required: true,
        },
        itemId: {
            type: 'String',
            required: true,
        },
        type: {
            type: 'String',
            required: true,
        },
        isSent: {
            type: 'Boolean',
            required: true,
        },
        channel: {
            type: 'String',
            required: true,
        },
        channelId: {
            type: 'String',
        },
        keyword: {
            type: 'String',
        },
    },
    {
        timestamps: true,
        collection: 'notifications',
    },
);

const Notification = mongoose.model('notifications', notificationSchema);
module.exports = Notification;
