/* eslint-disable camelcase */
const fs = require('fs');
const path = require('path');
const htmlparser2 = require('htmlparser2');
const parseItem = require('../../../src/sites/cyklobazar_cz/parsers/item');

const cyklobazar_cz_item_1 = require('./expected/cyklobazar_cz_item_1');

describe('Test the parser for cyklobazar_cz', () => {
    it('parsing of dom to item object', () => {
        const html = fs.readFileSync(path.resolve(__dirname, 'html/cyklobazar_cz_item_1.html'), { encoding: 'utf8' });
        const dom = htmlparser2.parseDocument(html);
        const url = new URL('https://www.cyklobazar.cz/inzerat/695831/trek-remedy-8-27-5');
        const item = parseItem(dom, url);
        expect(item).toEqual(cyklobazar_cz_item_1);
    });
});
