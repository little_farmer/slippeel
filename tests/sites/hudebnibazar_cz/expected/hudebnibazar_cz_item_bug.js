const hudebnibazar_cz_item_bug = {
    url: 'https://hudebnibazar.cz/sabian-xs-16-crash/ID494747/',
    itemId: 'ID494747',
    name: 'Sabian XS 16" crash.',
    text: 'Prodám crash medium thin Sabian XS 16"\nVelmi málo hraná. \nTop stav!',
    time: new Date('2023-01-18T13:35:00.000+00:00'),
    label: 'sell',
    price: 2500,
    currency: 'CZK',
    mainCategory: 'Bicí',
    subCategory: null,
    seller: {
        url: 'https://hudebnibazar.cz/uzivatel/detail/?ID=68395',
        id: '68395',
        name: 'Fandeni Fandeni',
        username: 'Fanden1',
        email: 'fandeni@seznam.cz',
        city: 'Praha 8',
        kraj: 'Praha',
        ratingPositive: 2,
        ratingNegative: 0,
    },
    pictures: [
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2023-01/f20a27d444b09141cf5268e95114c395.jpg',
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2023-01/af7da2cb69c9b0ef25e5221e5d94d1f6.jpg',
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2023-01/1e58ff3fc131970041be8aadad81628f.jpg',
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2023-01/481e429f4980a252d2a6cf72e06cc082.jpg',
        'https://hudebnibazar.cz/img_cache/1280x1280-0/img_inz/2023-01/7ef027a02edc2d7e3f843e7a1b3fb162.jpg',
    ],
};

module.exports = hudebnibazar_cz_item_bug;
