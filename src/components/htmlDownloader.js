const crypto = require('crypto');
const https = require('https');

// eslint-disable-next-line no-shadow
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));

async function fetchHtml(url, retryLimit, retryCount) {
    const rl = Math.abs(retryLimit, 0);
    const rc = Math.abs(retryCount, 0);
    const headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate, br',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'cross-site',
    };

    return fetch(url, {
        headers,
        body: null,
        method: 'GET',
        agent: new https.Agent({ secureOptions: crypto.constants.SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION }),
    })
        .then((res) => {
            if (res.status !== 200 && rc < rl) {
                return fetchHtml(url, rl, rc + 1);
            }
            return res.text();
        });
}

module.exports = { fetchHtml };
