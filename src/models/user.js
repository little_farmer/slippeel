const mongoose = require('mongoose');

const { Schema } = mongoose;

const channelsSchema = new Schema(
    {
        name: {
            type: 'String',
        },
        type: {
            type: 'String',
        },
        id: {
            type: 'String',
        },
    },
);

const userSchema = new Schema(
    {
        username: {
            type: 'String',
            required: true,
        },
        active: {
            type: 'Boolean',
            required: true,
        },
        role: {
            type: 'String',
            required: true,
        },
        channels: {
            type: [channelsSchema],
            required: false,
        },
    },
    {
        timestamps: true,
        collection: 'users',
    },
);

const User = mongoose.model('users', userSchema);
module.exports = User;
