// eslint-disable-next-line import/no-extraneous-dependencies
const domutils = require('domutils');

function parseList(dom) {
    return domutils
        .getElements({ class: 'InzeratBody' }, dom)
        .map((element) => domutils.getElements(
            {
                class: ':not(.InzeratBody)',
                tag_name: 'a',
            },
            element,
        ))
        .map((e) => new URL(domutils.getAttributeValue(e[e.length - 1], 'href'), 'https://hudebnibazar.cz/'));
}

module.exports = parseList;
