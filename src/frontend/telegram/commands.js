/* eslint-disable max-len */
/* eslint-disable no-lone-blocks */
const emoji = require('node-emoji');
const ejs = require('ejs');
const User = require('../../models/user');
const Item = require('../../models/item');
const Site = require('../../models/site');
const Watcher = require('../../models/watcher');

const adminChatId = process.env.TELEGRAM_ADMIN_CHAT_ID;

const replyArr = [];
async function listWatchers(msg, bot, opts, messageText) {
    let options = opts;
    const watchers = await Watcher.find({ channel: 'telegram', channelId: msg.chat.id }).exec();
    watchers
        .sort((a, b) => {
            if (a.keyword < b.keyword) return -1;
            if (a.keyword > b.keyword) return 1;
            return 0;
        });
    let keyword;
    const inline_keyboard = watchers
        .map((e) => {
            if (e.active) {
                keyword = {
                    text: `${emoji.get(':large_green_circle:')} ${e.keyword}`,
                    callback_data: `off ${e.keyword}`,
                };
            } else {
                keyword = {
                    text: `${emoji.get(':red_circle:')} ${e.keyword}`,
                    callback_data: `on ${e.keyword}`,
                };
            }
            return [
                keyword,
                { text: `${emoji.get(':wood:')} ${e.threshold}`, callback_data: `thr ${e.keyword}` },
                { text: `${emoji.get(':x:')}`, callback_data: `del ${e.keyword}` },
            ];
        });
    inline_keyboard.push([{ text: 'Close this menu', callback_data: 'close_list' }]);

    if (options) {
        options.parse_mode = 'HTML';
        options.reply_markup = {
            inline_keyboard,
        };
        bot.editMessageText(messageText || '/list for this chat', options);
    } else {
        options = {
            parse_mode: 'HTML',
            reply_markup: {
                inline_keyboard,
            },
        };
        bot.sendMessage(msg.chat.id, messageText || '/list for this chat', options);
    }
}

async function registerPrivate(bot, msg, match) {
    let username;
    if (match) username = match[1];
    else username = msg.from.username;

    if (!msg.from.username && !match) {
        bot.sendMessage(
            msg.chat.id,
            `${emoji.get(':x:')} please define your username after /register command, and try register again. '/register username'`,
        );
        return;
    }

    const user = await User.findOne(
        {
            'channels.name': 'telegram',
            'channels.type': 'private',
            'channels.id': msg.from.id,
        },
    );

    if (user) {
        if (user.active) {
            bot.sendMessage(msg.chat.id, `You are already registered ${user.username}`);
            return;
        }
        bot.sendMessage(msg.chat.id, `${emoji.get('snail')} Please wait for approval ${user.username}`);
        return;
    }

    const userWithUsername = await User.findOne(
        { username },
    );

    if (userWithUsername) {
        bot.sendMessage(
            msg.chat.id,
            `${emoji.get('dog')} Username already used, choose different '/register username'`,
        );
        return;
    }

    const newUser = {
        username,
        active: false,
        role: 'user',
        channels: [
            {
                name: 'telegram',
                type: 'private',
                id: msg.from.id,
            },
        ],
    };
    await User.updateOne({ username: newUser.username }, newUser, { upsert: true });

    // Message for new user
    bot.sendMessage(msg.chat.id, `${emoji.get('fishing_pole_and_fish')} please wait for approval from admin.`);

    // Message to admin group
    const inline_keyboard = [
        [
            {
                text: `${emoji.get(':white_check_mark:')} Approve`,
                callback_data: `approve ${msg.from.id}`,
            },
            {
                text: `${emoji.get(':x:')} Reject`,
                callback_data: `reject ${msg.from.id}`,
            },
        ],
        [{ text: 'Close this menu', callback_data: 'nothing' }],
    ];

    bot.sendMessage(adminChatId, `New user registration ${msg.from.id} @${msg.from.username}`, {
        reply_markup: {
            inline_keyboard,
        },
    });
}

async function registerGroup(bot, msg) {
    const user = await User.findOne({
        'channels.name': 'telegram',
        'channels.type': 'group',
        'channels.id': msg.chat.id,
    });

    if (user) {
        bot.sendMessage(msg.chat.id, `${emoji.get('octopus')} Bot for this group already registered.`);
        return;
    }

    const newChannelUser = await User.findOne({
        'channels.name': 'telegram',
        'channels.type': 'private',
        'channels.id': msg.from.id,
    });

    newChannelUser.channels.push({
        name: 'telegram',
        type: 'group',
        id: msg.chat.id,
    });

    await newChannelUser.save();
    bot.sendMessage(msg.chat.id, `${emoji.get('fried_shrimp')} Bot for this group registered!`);
}

async function userRegistered(bot, msg) {
    const user = await User.findOne({ 'channels.name': 'telegram', 'channels.id': msg.from.id, 'active': true });
    if (!user) {
        bot.sendMessage(msg.chat.id, 'You are not registered');
        return false;
    }
    return user;
}

function getPrivateChat(user) {
    return user.channels.reduce((acc, channel) => {
        if (channel.type === 'private' && channel.name === 'telegram') return channel.id;
        return acc;
    }, '');
}

function checkAdminRole(bot, msg, user) {
    if (user.role !== 'admin') {
        bot.sendMessage(msg.chat.id, 'You are not admin! Sudo won\'t help!');
        bot.sendMessage(adminChatId, `User ${msg.from.id} @${msg.from.username} used: ${msg.text}`);
        return false;
    }
    return true;
}

function commands(bot) {
    bot.onText(/\/register *$/, async (msg) => {
        if (msg.chat.type === 'private') registerPrivate(bot, msg);
        if (msg.chat.type === 'group') registerGroup(bot, msg);
    });

    bot.onText(/\/register (.+)/, async (msg, match) => {
        if (msg.chat.type === 'private') registerPrivate(bot, msg, match);
    });

    bot.onText(/\/start/, async (msg) => {
        const user = await userRegistered(bot, msg);
        if (!user) return;

        bot.sendMessage(msg.chat.id, 'Welcome, check /help for more info', {
            reply_markup: {
                keyboard: [['/list', '/help']],
                resize_keyboard: true,
            },
        });
    });

    bot.onText(/\/list/, async (msg) => {
        const user = await userRegistered(bot, msg);
        if (!user) return;

        listWatchers(msg, bot);
    });

    bot.onText(/\/add (.+)/, async (msg, match) => {
        const user = await userRegistered(bot, msg);
        if (!user) return;

        const keyword = match[1].trim().toLowerCase();
        if (keyword.length > 60) {
            bot.sendMessage(msg.chat.id, 'Keyword is too long, max is 60 characters');
            return;
        }

        const res = await Watcher.updateOne({
            user: user._id,
            keyword,
            channel: 'telegram',
            channelId: msg.chat.id,
        }, {
            user: user._id,
            active: true,
            keyword,
            channel: 'telegram',
            channelId: msg.chat.id,
            fuzzy: 0,
        }, { upsert: true });
        if (res.upsertedCount) bot.sendMessage(msg.chat.id, `Watcher '${keyword}' added`);
        if (res.modifiedCount) bot.sendMessage(msg.chat.id, `Watcher '${keyword}' already existing`);
    });

    bot.on('callback_query', async (callbackQuery) => {
        const action = callbackQuery.data.substring(0, callbackQuery.data.indexOf(' '));
        const keyword = callbackQuery.data.substring(callbackQuery.data.indexOf(' ') + 1);
        const msg = callbackQuery.message;
        const opts = {
            chat_id: msg.chat.id,
            message_id: msg.message_id,
        };

        if (callbackQuery.data === 'close_list') {
            bot.editMessageText('/list closed', opts);
            return;
        }

        const user = await User.findOne({
            'channels.name': 'telegram',
            'channels.id': callbackQuery.from.id,
        });

        let userQuery;
        if (action === 'approve' || action === 'reject') {
            userQuery = {
                'channels.name': 'telegram',
                'channels.type': 'private',
                'channels.id': keyword,
            };
        }
        let sentMessage;
        let res;
        let chatId;
        switch (action) {
        case 'on':
            res = await Watcher.updateOne({
                user: user._id,
                keyword,
                channel: 'telegram',
                channelId: msg.chat.id,
            }, { active: true }, { upsert: true });
            listWatchers(msg, bot, opts);
            break;
        case 'off':
            res = await Watcher.updateOne({
                user: user._id,
                keyword,
                channel: 'telegram',
                channelId: msg.chat.id,
            }, { active: false }, { upsert: true });
            listWatchers(msg, bot, opts);
            break;
        case 'thr':
            res = await Watcher.findOne({
                user: user._id,
                keyword,
                channel: 'telegram',
                channelId: msg.chat.id,
            });
            sentMessage = await bot.sendMessage(
                msg.chat.id,
                `<b>${keyword}</b> ${emoji.get(':wood:')} Reply to this message with new threshold. Decimal number in range [0..1]. 0.0 requires a perfect match, 1.0 would match almost anything.`,
                {
                    parse_mode: 'HTML',
                    reply_markup: {
                        force_reply: true,
                        input_field_placeholder: `${res.threshold}`,
                    },
                },
            );
            bot.editMessageText('/list closed', opts);
            replyArr.push({
                type: 'threshold',
                id: res._id,
                messageId: sentMessage.message_id,
            });
            break;
        case 'del':
            res = await Watcher.deleteOne({
                user: user._id,
                keyword,
                channel: 'telegram',
                channelId: msg.chat.id,
            });
            if (res.deletedCount) {
                listWatchers(msg, bot, opts, `Deleted: ${keyword}`);
            }
            break;
        case 'approve':
            res = await User.findOne(userQuery).exec();
            chatId = getPrivateChat(res);
            bot.editMessageText(`${emoji.get(':white_check_mark:')} ${callbackQuery.from.id} @${keyword} approved`, opts);
            bot.sendMessage(chatId, `${emoji.get(':frog:')} your registration was approved!`);
            await User.updateOne(userQuery, { active: true });
            break;
        case 'reject':
            res = await User.findOne(userQuery).exec();
            chatId = getPrivateChat(res);
            bot.editMessageText(`${emoji.get(':x:')} ${callbackQuery.from.id} @${keyword} approved`, opts);
            bot.sendMessage(chatId, `${emoji.get(':x:')} your registration was rejected, contact owner.`);
            await User.updateOne(userQuery, { active: false }, { upsert: true });
            break;
        default:
            break;
        }
    });

    bot.onText(/\/status/, async (msg) => {
        const user = await userRegistered(bot, msg);
        if (!checkAdminRole(bot, msg, user)) return;

        const sites = await Site.find();

        for await (const site of sites) {
            site.count = await Item.count({ site: site.site });
        }

        try {
            ejs.renderFile(
                `${__dirname}/templates/status.ejs`,
                { sites },
                (err, res) => {
                    if (err) throw Error(err);
                    bot.sendMessage(msg.chat.id, res, { parse_mode: 'HTML' });
                },
            );
        } catch (err) {
            console.error('Telegram Api Error: sending /status');
        }
    });

    bot.on('message', async (msg) => {
        if (msg.reply_to_message && replyArr.length) {
            let sentMessage;
            let threshold = Number(msg.text.replaceAll(',', '.'));
            const replyIndex = replyArr.findIndex((e) => e.messageId === msg.reply_to_message.message_id);
            const reply = replyArr.splice(replyIndex, 1);
            const watcher = await Watcher.findOne({ _id: reply[0].id });
            if (Number.isNaN(threshold) || threshold < 0 || threshold > 1) {
                bot.sendMessage(
                    msg.chat.id,
                    `${emoji.get(':x:')} Not a number in range [0..1], please try again`,
                );
                sentMessage = await bot.sendMessage(
                    msg.chat.id,
                    `<b>${watcher.keyword}</b> ${emoji.get(':wood:')} Reply to this message with new threshold. Decimal number in range [0..1]. 0.0 requires a perfect match, 1.0 would match almost anything.`,
                    {
                        parse_mode: 'HTML',
                        reply_markup: {
                            force_reply: true,
                            input_field_placeholder: `${watcher.threshold}`,
                        },
                    },
                );
                replyArr.push({
                    type: 'threshold',
                    id: watcher._id,
                    messageId: sentMessage.message_id,
                });
                return;
            }
            threshold = threshold.toFixed(2);
            watcher.threshold = threshold;
            await watcher.save();
            listWatchers(msg, bot, null, `/list ${watcher.keyword} threshold set to: ${msg.text}`);
        }
    });
}

module.exports = commands;
