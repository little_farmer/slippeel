// eslint-disable-next-line import/no-extraneous-dependencies
const domutils = require('domutils');
const cyklobazarCategories = require('../static/categories');
const { properties } = require('../static/properties');

function parseDatetimes(datetime) {
    const dateParts = datetime.split(' ')[1].slice(0, -1).split('.');
    const timeParts = datetime.split(' ')[2].split(':');

    return new Date(dateParts[2], dateParts[1] - 1, dateParts[0], timeParts[0], timeParts[1]);
}

function parseItem(dom, url) {
    const host = 'https://www.cyklobazar.cz';
    const item = {};
    let startIndex = 0;

    // Item URL
    item.url = url.href;
    // Item ID
    item.itemId = url.pathname.split('/')[2];

    // Item Name
    item.name = domutils
        .innerText(domutils.getElementsByTagName('h1', dom))
        .trim();

    // Item advertisement text
    item.text = domutils
        .innerText(
            domutils.getElements({ class: 'offer-detail__desc' }, dom),
        )
        .trim()
        .substring(15);

    // Advertisement date
    const itemDatetime = domutils
        .innerText(domutils.getElements({ class: 'cb-property-box__footer__table' }, dom))
        .trim()
        .split('   ');
    itemDatetime.pop();

    // const itemDateCreated = parseDatetimes(itemDatetime[0]);
    const itemDateEdited = parseDatetimes(itemDatetime[1]);
    item.time = itemDateEdited;

    if (itemDatetime.length === 3) {
        const itemDateBoosted = parseDatetimes(itemDatetime[2]);
        item.time = itemDateBoosted;
    }

    // Price
    const itemRawPrice = domutils
        .getElements({ class: 'cb-seller-box__price' }, dom)[0].children;

    item.price = Number(itemRawPrice[0].data.trim().replace(' ', ''));

    if (Number.isNaN(item.price)) {
        item.price = null;
    } else {
        const itemRawCurrency = itemRawPrice[1].children[0].data;
        switch (itemRawCurrency) {
        case 'Kč':
            item.currency = 'CZK';
            break;
        case '€':
            item.currency = 'EUR';
            break;
        case 'zł':
            item.currency = 'PLN';
            break;
        default:
            break;
        }
    }

    // Property table
    const itemRawTable = domutils
        .getElements({ class: 'cb-property-box__table' }, dom)[0].children;

    const resultArr = itemRawTable.reduce((rows, row) => {
        if (!row.name) return rows;
        const columns = row.children.reduce((acc, column) => {
            if (column.name === 'th') {
                return {
                    property: column.children[0].data.slice(0, -1),
                    value: column.next.next.children[1].children[0].data.trim(),
                };
            }
            return acc;
        }, []);
        return [...rows, columns];
    }, []);

    // Type of advertisement
    startIndex = resultArr.findIndex((o) => o.property === 'Typ inzerce');
    if (startIndex >= 0) {
        const itemRawLabel = resultArr.splice(startIndex, 1)[0];
        switch (itemRawLabel.value) {
        case 'Nabídka':
            item.label = 'sell';
            break;
        case 'Poptávka':
            item.label = 'buy';
            break;
        default:
            item.label = 'other';
            break;
        }
    }

    // Category
    startIndex = resultArr.findIndex((o) => o.property === 'Kategorie');
    if (startIndex >= 0) {
        const itemRawCategory = resultArr.splice(startIndex, 1)[0];
        item.subCategory = itemRawCategory.value;
        item.mainCategory = cyklobazarCategories[itemRawCategory.value];
    }

    // Properties
    if (resultArr.length) {
        item.properties = {};
        resultArr.forEach((prop) => {
            item.properties[properties[prop.property]] = prop.value;
        });
    }

    // Seller whole info

    // Seller url
    let sellerInfo = domutils.getElements({ class: 'cb-seller-box__seller cb-seller-box__seller--photo' }, dom);
    const urlSellerRaw = domutils.getElementsByTagName('a', sellerInfo);
    if (urlSellerRaw.length > 0) {
        item.seller = {};
        const urlSeller = new URL(urlSellerRaw[0].attribs.href, host);
        item.seller.url = urlSeller.href;

        // Seller ID
        item.seller.id = urlSeller.pathname.split('/')[2];

        // Seller name
        sellerInfo = domutils.getElements({ class: 'cb-seller-box__title' }, dom);
        item.seller.name = sellerInfo[0].children[0].data.trim();

        // Seller location
        sellerInfo = domutils.getElements({ class: 'cb-seller-box__seller' }, dom);
        const sellerLocationRaw = sellerInfo[0].children[0].data.trim();
        if (sellerLocationRaw) {
            item.seller.city = sellerLocationRaw.split(',')[0];
            item.seller.kraj = sellerLocationRaw.split(',')[1].trim();
        }

        // Seller telephone
        const regexTelephone = /\+(\d|\s){4,}/;
        const telephone = regexTelephone.exec(sellerInfo);

        if (telephone) item.seller.telephone = telephone[0].trim();
    }

    // Item Pictures
    const itemPictures = domutils.getElementsByTagName(
        'a',
        domutils.getElements({ class: 'SwipeGallery__carousel' }, dom),
    );

    if (itemPictures.length) {
        item.pictures = itemPictures.map((e) => host + domutils.getAttributeValue(e, 'href'));
    }

    return item;
}

module.exports = parseItem;
