const { existsSync } = require('fs');
const path = require('path');
const ejs = require('ejs');

function templateFile(item, notification) {
    const filepath = path.join(
        __dirname,
        '../../sites',
        item.site,
        'templates',
        `/${notification.type}Item.ejs`,
    );
    if (existsSync(filepath)) {
        return filepath;
    }
    return `${__dirname}/templates/${notification.type}Item.ejs`;
}

function formatItemPrice(item) {
    if (item.price) {
        return new Intl.NumberFormat(
            'cs-CZ',
            {
                style: 'currency',
                currency: item.currency,
            },
        ).format(item.price);
    }

    return 'negotiable';
}

function sendPictures(bot, notification, item, queue) {
    if (item.pictures.length < 1) return;
    if (item.pictures.length === 1) {
        queue.enqueue(() => bot.sendPhoto(notification.channelId, item.pictures[0], { caption: item.name }));
        return;
    }

    const pictures = item.pictures.map((picture, index) => {
        if (index % 10 === 0) {
            return { type: 'photo', media: picture, caption: item.name };
        }
        return { type: 'photo', media: picture };
    });

    while (pictures.length > 0) {
        const subPictures = pictures.splice(0, 10);
        queue.enqueue(() => bot.sendMediaGroup(notification.channelId, subPictures));
    }
}

function sendNewItem(bot, notification, item, queue) {
    const keyword = notification.keyword;
    const price = formatItemPrice(item);

    try {
        ejs.renderFile(
            templateFile(item, notification),
            { item, price, keyword },
            (err, res) => {
                if (err) throw Error(err);
                queue.enqueue(() => bot.sendMessage(notification.channelId, res, { parse_mode: 'HTML' }));
            },
        );
        if (item.pictures) sendPictures(bot, notification, item, queue);
    } catch (err) {
        console.error(err, 'Telegram Api Error: sending images');
    }
}

function sendChangedItem(bot, notification, item, queue) {
    const keyword = notification.keyword;
    const snapshot = item.snapshots.reduce((itemSnapshot, acc) => {
        if (itemSnapshot.time.getTime() > acc.time.getTime()) {
            return itemSnapshot;
        }
        return acc;
    }, item.snapshots[0]);

    const datetimeFormat = {
        year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric',
    };
    const changeTime = snapshot.time.toLocaleDateString('cs-CZ', datetimeFormat);

    if ('price' in snapshot) {
        if (snapshot.price) {
            snapshot.price = new Intl.NumberFormat('cs-CZ', { style: 'currency', currency: item.currency }).format(snapshot.price);
        } else {
            snapshot.price = 'negotiable';
        }
    }
    const price = formatItemPrice(item);

    const excludeFromNotifying = ['time', 'url', 'pictures', 'seller'];
    excludeFromNotifying.forEach((e) => delete snapshot[e]);
    const changes = Object.entries(snapshot);
    if (!changes.length) return;

    try {
        ejs.renderFile(
            templateFile(item, notification),
            {
                item, price, keyword, changeTime, changes,
            },
            (err, res) => {
                if (err) throw Error(err);
                queue.enqueue(() => bot.sendMessage(notification.channelId, res, { parse_mode: 'HTML' }));
            },
        );

        if (item.pictures) sendPictures(bot, notification, item, queue);
    } catch {
        console.error('Telegram Api Error: sending message');
    }
}

module.exports = {
    sendNewItem,
    sendChangedItem,
};
