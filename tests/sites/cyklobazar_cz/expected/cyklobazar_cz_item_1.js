/* eslint-disable max-len */
const cyklobazar_cz_item_1 = {
    itemId: '695831',
    currency: 'CZK',
    label: 'sell',
    mainCategory: 'Kola',
    name: 'Trek Remedy 8, 27,5"',
    pictures: [
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142206_6ae213994.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142159_062876c5b.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142218_d0992917a.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142227_eb1f7593e.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142310_8c6a89b3d.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142236_1d52310aa.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142341_b381e7b31.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142326_44e3e6b3c.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142343_0995c670a.jpg',
        'https://www.cyklobazar.cz/uploads/items/2023/3/14/695831/img-20230314-142406_76d1078d6.jpg',
    ],
    price: 66000,
    properties: {
        maker: 'Trek',
        color: 'Černá',
        material: 'Alu - hliník',
        size: 'L / 19-20" / do 52 cm / 170-185 cm',
        wheelDiameter: '27,5"',
        yearModel: '2019',
    },
    subCategory: 'Horská kola',
    text: 'Kvůli nedostatku času, prodávám své kolo Trek Remedy 8, 27,5". Velikost L - 19,5. Model 2019. Kupováno 1/2019 Kolo má najeto cca 1000 km, pravidelně servisováno, bez investic, nachystáno na sezónu. Výbava: Vydlice - RockShox Lyric 160 cm Zadní tlumič - RockShox deluxe debon air reactiv. Přehazovačka - Sram GX Eagle / 12ti kolečko 10-50 Kotouče Sram přední 200mm / zadní 180 mm Oproti standartní výbavě má kolo brzdy čtyř pístkové Shimano ZEE.  Cena k jednání',
    time: new Date(2023, 2, 14, 15, 18),
    url: 'https://www.cyklobazar.cz/inzerat/695831/trek-remedy-8-27-5',
};

module.exports = cyklobazar_cz_item_1;
