# SLIPPEEL

## Description

### Why

-   This project was developed to notify me and my friends asap on new offers at sites with used goods. We missed good deals beacause we discovered them too late.
-   I used this project as learn new things for me.
-   To keep history of offers on various items and prices through time.

### How

-   After scheduled and periodical scraping of site for new offers i store them in MongoDB.
-   For frontend is used Telegram bot.
-   Every user can register and manage his/her own keywords.
-   When is detected any active keyword in new offers, the users with defined keyword get notification on Telegram with pictures.
-   The changes on offers are also checked and stored right on existing offer as snapshot, with timestamp. Users also get notificitaion on existing offers with changed price or text.
-   Notifications are HTML templates, that use npm EJS templating packageand show main information about offer.
-   Scraping scripts can be used as standalone services, and use normalized output for returned offers.
-   Goal is to keep this application modular, so anybody who write scraper for site, can easily connect it to this application.
-   I have this application at my home docker enviroment on Raspberry PI and i deploy on it using Gitlab pipelines.

### Scrapers

[x] hudebnibazar.cz

[x] cyklobazar.cz

[ ] fotoskoda.cz/bazar

[ ] paladix.cz/bazar

[ ] midi.cz

[ ] bazos.cz

## To Do

[x] Scraper for cyklobazar.cz

[x] User registration with approval from admin

[x] Rework notifications to work with group chats

[ ] Do a better error handling

[ ] Add logging system

[ ] Rewrite changes detection as module and not as part of runner for scraping

[ ] Scraper for bazar.fotoskoda.cz

[ ] Website frontend for users

## Credits

[xangox](https://gitlab.com/xangox) for consultations.

My wife for logo.
