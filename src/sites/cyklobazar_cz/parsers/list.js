// eslint-disable-next-line import/no-extraneous-dependencies
const domutils = require('domutils');

function parseList(dom) {
    return domutils
        .getElements({ class: 'cb-offer' }, dom)
        .map((element) => domutils.getElements({ tag_name: 'a' }, element))
        .map((e) => new URL(domutils.getAttributeValue(e[e.length - 1], 'href'), 'https://www.cyklobazar.cz/'));
}

module.exports = parseList;
