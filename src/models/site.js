const mongoose = require('mongoose');

const { Schema } = mongoose;

const itemSchema = new Schema(
    {
        site: {
            type: 'String',
            required: true,
        },
        url: {
            type: 'String',
            required: true,
        },
        urlForScrape: {
            type: 'String',
            required: true,
        },
        excludeFieldsForSnapshots: {
            type: ['String'],
        },
        execution: {
            status: {
                type: 'String',
            },
            pageSize: {
                type: 'Number',
            },
            concurrency: {
                type: 'Number',
            },
            intervalMinutes: {
                type: 'Number',
            },
            lastItemTime: {
                type: 'Date',
            },
            lastItemId: {
                type: 'Number',
            },
            lastExecution: {
                type: 'Date',
            },
            lastTouch: {
                type: 'Date',
            },
            lastFinish: {
                type: 'Date',
            },
            lastPageIndex: {
                type: 'Number',
            },
        },
    },
    {
        timestamps: true,
        collection: 'sites',
    },
);

const Site = mongoose.model('sites', itemSchema);
module.exports = Site;
