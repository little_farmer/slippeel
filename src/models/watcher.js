const mongoose = require('mongoose');

const { Schema } = mongoose;

const watcherSchema = new Schema(
    {
        user: {
            type: 'ObjectId',
            required: true,
        },
        active: {
            type: 'Boolean',
            required: true,
        },
        keyword: {
            type: 'String',
            required: true,
        },
        channel: {
            type: 'String',
            required: true,
        },
        channelId: {
            type: 'String',
            required: true,
        },
        threshold: {
            type: 'Number',
        },
        fuzzy: {
            type: 'Number',
        },
    },
    {
        timestamps: true,
        collection: 'watchers',
    },
);

const Watcher = mongoose.model('watchers', watcherSchema);
module.exports = Watcher;
