function itemChangeFinder(oldItem, newItem, notToCompare) {
    const keys = Object.keys(newItem);
    const oldTime = oldItem.time;
    const snapshot = {};
    keys.forEach((key) => {
        if (!notToCompare.includes(key) && key === 'pictures') {
            if (oldItem.pictures.toString() !== newItem.pictures.toString()) {
                snapshot[key] = oldItem[key];
            }
        } else if (!notToCompare.includes(key) && oldItem[key] !== newItem[key]) {
            snapshot[key] = oldItem[key];
        }
    });
    if (Object.keys(snapshot).length) {
        snapshot.time = oldTime;
        return snapshot;
    }
    return false;
}

module.exports = itemChangeFinder;
