const Item = require('../models/item');
const itemChangeFinder = require('./itemChangeFinder');

async function upsertOneItem(item, siteDoc) {
    const notToCompare = [...siteDoc.excludeFieldsForSnapshots];
    notToCompare.push('snapshots');

    const oldItem = await Item.findOne({ itemId: item.itemId, site: siteDoc.site }).exec();
    let snapshot;

    if (oldItem) {
        snapshot = itemChangeFinder(oldItem, item, notToCompare);
        if (snapshot) {
            item.snapshots = [...(oldItem.snapshots || []), snapshot];
        }
    }

    if (snapshot || !oldItem) {
        await Item.updateOne({ itemId: item.itemId, site: siteDoc.site }, item, { upsert: true });
    }
}

module.exports = { upsertOneItem };
